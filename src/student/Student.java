/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

/**
 *
 * @author eshka
 */
public class Student extends Person{
    private int studentId;
    //constructor

    public Student(String firstName, String lastName, int studentId) {
        super(firstname,lastname);
        this.studentId = studentId;
    }
    //getter and setter

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", Id: " + studentId ;
    }
}
